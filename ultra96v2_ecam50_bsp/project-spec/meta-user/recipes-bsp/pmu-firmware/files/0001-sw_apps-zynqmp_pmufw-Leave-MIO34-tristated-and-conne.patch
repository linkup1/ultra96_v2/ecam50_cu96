From efeb0a5ae2a059f7e3f60b387cb967b30f045266 Mon Sep 17 00:00:00 2001
From: Mounika Grace Akula <makula@xilinx.com>
Date: Fri, 25 Jan 2019 17:11:49 +0530
Subject: [EMBEDDEDSW PATCH] sw_apps: zynqmp_pmufw: Leave MIO34 tristated and
 connect it to PMU GPO when it needs to be driven low

When POR is asserted, MIO34 is driven low due to GPO being driven low.
To avoid this, leave MIO34 tristated and connect it to PMU GPO when
MIO34 actually needs to be driven low.

Signed-off-by: Mounika Grace Akula <makula@xilinx.com>
---
CR:1020504

 lib/sw_apps/zynqmp_pmufw/src/pm_config.c    | 16 ++++++----------
 lib/sw_apps/zynqmp_pmufw/src/pm_hooks.c     |  5 ++++-
 lib/sw_apps/zynqmp_pmufw/src/xpfw_default.h |  4 ++++
 3 files changed, 14 insertions(+), 11 deletions(-)

diff --git a/lib/sw_apps/zynqmp_pmufw/src/pm_config.c b/lib/sw_apps/zynqmp_pmufw/src/pm_config.c
index 5191911..385d98b 100644
--- a/lib/sw_apps/zynqmp_pmufw/src/pm_config.c
+++ b/lib/sw_apps/zynqmp_pmufw/src/pm_config.c
@@ -1,5 +1,5 @@
 /*
- * Copyright (C) 2014 - 2016 Xilinx, Inc.  All rights reserved.
+ * Copyright (C) 2014 - 2019 Xilinx, Inc.  All rights reserved.
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
@@ -54,11 +54,6 @@ static s32 PmConfigGpoSectionHandler(u32* const addr);
  * Macros
  ********************************************************************/
 
-#define IOU_SLCR_MIO_PIN_34_OFFSET	0x00000088
-#define IOU_SLCR_MIO_PIN_35_OFFSET	0x0000008C
-#define IOU_SLCR_MIO_PIN_36_OFFSET	0x00000090
-#define IOU_SLCR_MIO_PIN_37_OFFSET	0x00000094
-
 #define PM_CONFIG_GPO_2_ENABLE_MASK	BIT(10U)
 #define PM_CONFIG_GPO_3_ENABLE_MASK	BIT(11U)
 #define PM_CONFIG_GPO_4_ENABLE_MASK	BIT(12U)
@@ -484,10 +479,11 @@ static s32 PmConfigGpoSectionHandler(u32* const addr)
 	reg |= (gpoState & PM_CONFIG_GPO_MASK);
 	XPfw_Write32(PMU_IOMODULE_GPO1, reg);
 
-	if (gpoState & PM_CONFIG_GPO_2_ENABLE_MASK) {
-		XPfw_RMW32((IOU_SLCR_BASE + IOU_SLCR_MIO_PIN_34_OFFSET),
-				0x000000FEU, 0x00000008U);
-	}
+	/*
+	 * Leave MIO34 tristated to avoid a glitch on MIO34 when GPO1[2] 
+	 * is driven low during POR assertion. PMU will connect MIO34 to
+	 * GPO1[2] when it needs to be driven low.
+	 */
 
 	if (gpoState & PM_CONFIG_GPO_3_ENABLE_MASK) {
 		XPfw_RMW32((IOU_SLCR_BASE + IOU_SLCR_MIO_PIN_35_OFFSET),
diff --git a/lib/sw_apps/zynqmp_pmufw/src/pm_hooks.c b/lib/sw_apps/zynqmp_pmufw/src/pm_hooks.c
index fc1b198..115450a 100644
--- a/lib/sw_apps/zynqmp_pmufw/src/pm_hooks.c
+++ b/lib/sw_apps/zynqmp_pmufw/src/pm_hooks.c
@@ -1,5 +1,5 @@
 /*
- * Copyright (C) 2017 - 2018 Xilinx, Inc.  All rights reserved.
+ * Copyright (C) 2017 - 2019 Xilinx, Inc.  All rights reserved.
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
@@ -132,6 +132,9 @@ void PmHookPowerDownLpd(void)
 	reg = XPfw_Read32(PMU_LOCAL_GPO1_READ);
 	reg &= ~PMU_IOMODULE_GPO1_MIO_2_MASK;
 	XPfw_Write32(PMU_IOMODULE_GPO1, reg);
+	/* Configure MIO34 to be controlled by the PMU */
+	XPfw_RMW32((IOU_SLCR_BASE + IOU_SLCR_MIO_PIN_34_OFFSET),
+			0x000000FEU, 0x00000008U);
 }
 
 #ifdef ENABLE_DDR_SR_WR
diff --git a/lib/sw_apps/zynqmp_pmufw/src/xpfw_default.h b/lib/sw_apps/zynqmp_pmufw/src/xpfw_default.h
index 677e360..513b845 100644
--- a/lib/sw_apps/zynqmp_pmufw/src/xpfw_default.h
+++ b/lib/sw_apps/zynqmp_pmufw/src/xpfw_default.h
@@ -56,6 +56,10 @@ extern "C" {
 
 /* Base address of the IOU_SLCR module */
 #define IOU_SLCR_BASE			0xFF180000U
+#define IOU_SLCR_MIO_PIN_34_OFFSET	0x00000088
+#define IOU_SLCR_MIO_PIN_35_OFFSET	0x0000008C
+#define IOU_SLCR_MIO_PIN_36_OFFSET	0x00000090
+#define IOU_SLCR_MIO_PIN_37_OFFSET	0x00000094
 
 /* RAM address used for scrubbing */
 #define PARAM_RAM_LOW_ADDRESS		0Xffdc0000U
-- 
1.9.5

