/include/ "system-conf.dtsi"
/include/ "openamp.dtsi"

#include <dt-bindings/media/xilinx-vip.h>

/ { 
   sdio_pwrseq: sdio_pwrseq {  /* This needs to be able to manipulate the chip_en and the resetn properly */
      compatible = "mmc-pwrseq-simple";
      /* post-power-on-delay-ms = <10>; */
      reset-gpios = <&gpio 7 1>; // MIO[7] for WILC3000 RESETN, Active low
      /* reset-gpios = <&gpio 78 1>;  // Some prototype boards only! device pin A3 for RESETN, Active low */
      chip_en-gpios = <&gpio 8 1>; 
   };

   // Remove V1 Power ON/OFF controller from U96 V1 DT
   /delete-node/ ltc2954;

	aliases {
		i2c0 = &i2c1;
   };
 
   cam_clk: cam_clk {
        #clock-cells = <0>;
        compatible = "fixed-clock";
        clock-frequency = <25000000>;
   };
};

&uart0 {
   // Remove TI child node from U96 V1 DT
   /delete-node/bluetooth;
};

&gpio {
   /delete-property/gpio-line-names;
};

&sdhci1 {
   bus-width= <0x4>;
   /* non-removeable; */
   /* mmc-pwrseq = <&sdio_pwrseq>; */
   max-frequency = <50000000>;
   /delete-property/cap-power-off-card; // This is not compatible with the WILC3000 and means the WILC will always be powered on
   status = "okay";
   #address-cells = <1>;
   #size-cells = <0>;
   wilc_sdio@1 {
      compatible = "microchip,wilc3000", "microchip,wilc3000";
      reg = <0>;
      // interrupt-parent = <&gpio>; // Microchip driver DOES NOT use gpio irq any more!
      // interrupts = <76 2>; /* MIO76 WILC IRQ 1V8 */ // Microchip driver DOES NOT use gpio irq any more!
      // irq-gpios = <&gpio 76 0>; // Microchip driver DOES NOT use gpio irq any more!
      bus-width= <0x4>;
      status = "okay";
   };
   // Remove TI child node from U96 V1 DT
   /delete-node/wifi@2;
};

&i2csw_2 {
    ar0521_mipi: ar0521_mipi@42 {
		compatible = "aptina,ar0521_mcu";
		reg = <0x42>;
		clocks = <&cam_clk>;
		reset-gpios = <&gpio 80 0>;
		pwdn-gpios = <&gpio 79 0>;
		status = "okay";
		port {
			sensor_out_0: endpoint {
				remote-endpoint = <&csiss_in>;
				clock-lanes = <0>;
				data-lanes = <1 2 3 4>;
			};
		};
	};
};

&usb0 {
   status = "okay";
};

&dwc3_0 {
   status = "okay";
   dr_mode = "peripheral";
   phy-names = "usb3-phy";
	phys = <&lane2 4 0 0 26000000>;
	maximum-speed = "super-speed";   
};

&usb1 {
   status = "okay";
};

&dwc3_1 {
   status = "okay";
	dr_mode = "host";
	phy-names = "usb3-phy";
	phys = <&lane3 4 1 0 26000000>;
	maximum-speed = "super-speed";
};

&mipi_csi2_rx_subsyst_0 {
    ports {
        #address-cells = <1>;
        #size-cells = <0>;
 
        port@0 {
            xlnx,video-format = <XVIP_VF_YUV_422>;
        };
        port@1 {
            xlnx,video-format = <XVIP_VF_YUV_422>;
            csiss_in: endpoint {
                data-lanes = <1 2 3 4>;
                remote-endpoint = <&sensor_out_0>;
            };
        };
    };
};
 
&v_frmbuf_wr_0 {
    xlnx,vid-formats = "yuyv", "uyvy", "y8";
};
