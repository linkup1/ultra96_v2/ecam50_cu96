#!/bin/sh

#Get system date
today=`date +%Y-%m-%d.%H:%M:%S`
filename="/home/root/$today.txt"
echo $filename

#launch camera module
if lsmod | grep "ar0521_mcu" &> /dev/null ; then
	echo "Module already loaded"
else
	modprobe ar0521_mcu
	rc=$?
	if [[ $rc != 0 ]]; then 
		echo "Failed to load camera module..."
		exit $rc 
	fi
fi

sleep 5

#Configure media ctrl entities
media-ctl -v -V '"bf000000.mipi_csi2_rx_subsystem":1 [fmt:UYVY8_1X16/1920x1080]'
rc=$?
if [[ $rc != 0 ]]; then exit $rc; fi

sleep 1
media-ctl -v -V '"bf000000.mipi_csi2_rx_subsystem":0 [fmt:UYVY8_1X16/1920x1080]'
rc=$?
if [[ $rc != 0 ]]; then exit $rc; fi

sleep 1
media-ctl -v -V '"ar0521_mcu 4-0042":0 [fmt:UYVY8_1X16/1920x1080]'
rc=$?
if [[ $rc != 0 ]]; then exit $rc; fi

#Start gstremaer
gst-launch-1.0 v4l2src device=/dev/video0 io-mode="dmabuf" ! "video/x-raw, width=1920,height=1080,framerate=60/1" ! fpsdisplaysink video-sink="kmssink bus-id=fd4a0000.zynqmp-display fullscreen-overlay=true" text-overlay=false sync=false -v 2>&1 | tee $filename &
rc=$?
if [[ $rc != 0 ]]; then 
echo "Failed to Start Stream..."
exit $rc 
fi

