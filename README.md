## ecam50_CU96 camera driver for Ultra96_V2  
Copyright © 2020 e-con Systems India Pvt. Limited All rights reserved.  

####VERSION  

LinkUP_u96_V1.0  

####FEATURES:  

e-con systems now provide camera modules for Xilinx Ultra96_V2 boards.  

Ultra96-V2 is an Arm-based, Xilinx Zynq UltraScale+ ™ MPSoC development board based on the Linaro 96Boards Consumer Edition (CE) specification.  

e-CAM55_CUMI0521_MOD camera module is a 5MP camera with MIPI connector designed and developed by e-con systems.    

Camera module(e-CAM55_CUMI0521_MOD), camera adapter board (e-CAM130_TRICUTX2_ADAPTOR) and camera base board (ACC_96DEV_MIPICAMERA_ADP) are collectively called as e-CAM50_CU96.  

1.e-CAM55_CUMI0521_MOD camera driver supports following resolutions,  

	1280x720 @45fps  
	1920x1080 @60fps  
	2560x1440 @35fps  

2.Image quality controls to enable adjustments  

	Brightness  
	Contrast  
	Saturation  
	Gamma  
	Gain  
	Sharpness  
	White_balance controls  
	Flip  
	Tilt and Pan  
 	Zoom  
	Exposure - Auto,Manual,Roi based  
	Exposure compensation  
	Denoise  

####RELEASE_CONTENTS  

	ecam50_cu96  
		-- ultra96v2_ecam50_bsp - BSP Source to build binaries for Xilinx Ultra96_V2 Board.  
		-- pre_built_binaries   - Contains pre-built binary files inside "Linkup_U96_v1_0_Bin.tar.bz2" file to quickly test the release.  
		-- BUILD_README         - Contains steps to setup the system to build images and prepare bootable SD Card.  
		-- CONTROLS_README      - Contains steps to configure media entity, stream camera and configure camera controls.  
		-- README.md            - You are reading it.  

####PREREQUISITES  

	1. Ultra96_V2 Board  
	2. e-CAM55_CUMI0521_MOD (REV X4)  
	3. e-CAM130_TRICUTX2_ADAPTOR (REV B)  
	4. ACC_96DEV_MIPICAMERA_ADP (REV X1) camera adapter developed by e-con systems.  
